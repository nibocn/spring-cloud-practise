package me.nibo.springcloud.stream.produce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 服务启动类
 *
 * @author NiBo
 */
@SuppressWarnings({"PMD.UseUtilityClass", "PMD.ClassNamingConventions"})
@SpringBootApplication
public class ProduceApplication {

    // CSOFF: Javadoc
    public static void main(String[] args) {
        SpringApplication.run(ProduceApplication.class, args);
    }
    // CSON: Javadoc

}
