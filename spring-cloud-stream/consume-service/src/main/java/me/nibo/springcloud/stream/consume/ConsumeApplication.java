package me.nibo.springcloud.stream.consume;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 服务启动类
 *
 * @author NiBo
 */
@SuppressWarnings({"PMD.UseUtilityClass", "PMD.ClassNamingConventions"})
@SpringBootApplication
public class ConsumeApplication {

    // CSOFF: Javadoc
    public static void main(String[] args) {
        SpringApplication.run(ConsumeApplication.class, args);
    }
    // CSON: Javadoc

}
